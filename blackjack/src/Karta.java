/**
 * Created by Krzysztof on 2017-03-14.
 */

public class Karta {
private Kolor kolor;
private Ranking ranking;

public Karta(Kolor kolor, Ranking ranking) {
    this.ranking=ranking;
    this.kolor=kolor;
}

public Ranking wartosc (){
    return  this.ranking;
}

public String toString()
{
    return  this.kolor.toString() + " - " + this.ranking.toString();
}
}
