/**
 * Created by Krzysztof on 2017-03-15.
 */

public enum Kolor {
    PIK, //- spades - pik
    KIER, //- hearts - kier
    KARO, //- diamonds - karo
    TREFL //- clubs - trefl
}
