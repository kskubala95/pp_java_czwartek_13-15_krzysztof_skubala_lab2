import java.util.Scanner;

/**
 * Created by Krzysztof on 2017-03-09.
 */

/**
 * przeladowanie metody ... przeciazenie metody
 **/
////klasy
///////////////////// karta (kolor Wartosc_Talii)  "dekodowanie wartosci kart walet .... itd
/// // reka()
/// // rozzgrywka (kazde dziedziczy z poprzedniego)

public class BlackJack /* extends Talia*/ {
    public static void main( String[] args ) {

        double konto = 0;
        Talia bierzaca_talia = new Talia( );
        bierzaca_talia.Stworz_Talie( );
        bierzaca_talia.tasuj( );
        Talia Reka = new Talia( );
        Talia Krupier = new Talia( );
        Scanner cin = new Scanner( System.in );
        System.out.println( "Stan Twojego konta wynocsi: " + konto + ". Czy chcesz je zasilic ? (flase - nie ; true - tak)" );
        boolean czy = cin.nextBoolean( ); // czy chcesz zasilic konto
        boolean czy2 = false; // czy zaklad jest poprawny
        double zaklad = -1;
        boolean gramy = true;
        if ( czy == true ) {
            System.out.println( "Jaka kwota chcesz zasilic konto ?" );
            konto = cin.nextDouble( );
            while ( konto > 0 && gramy == true ) {
                boolean Runda = false;
                while ( czy2 != true ) {
                    System.out.println( "Obstaw rozgrywke !" );
                    zaklad = cin.nextDouble( );
                    if ( zaklad > konto ) {
                        System.out.println( "Nie posiadasz tyle na koncie." );
                    } else if ( zaklad < 0 ) {
                        System.out.println( "Musisz cos postawic." );
                    } else {
                        System.out.println( "Obstawiono. " );
                        czy2 = true;
                    }
                }
                Reka.Dobierz( bierzaca_talia );
                Reka.Dobierz( bierzaca_talia );
                Krupier.Dobierz( bierzaca_talia );
                Krupier.Dobierz( bierzaca_talia );

                while ( true ) {
                    System.out.println( "Reka:" + Reka.toString( ) );
                    System.out.println( "Wartosc reki: " + Reka.Wartosc_Talii( ) );
                    System.out.println( "Reka krupiera: " + Krupier.zwroc_karte( 0 ).toString( ) + " oraz karta ukryta. " );
                    System.out.println( "(1)Dobierz - (2)Nie dobieraj" );
                    int a = cin.nextInt( );
                    if ( a == 1 ) {
                        Reka.Dobierz( bierzaca_talia );
                        System.out.println( "Pobrales:" + Reka.zwroc_karte( Reka.rozmiar_talii( ) - 1 ).toString( ) );
                        if ( Reka.Wartosc_Talii( ) > 21 ) {
                            System.out.println( "Przegrales - przekroczyles 21 : " + Reka.Wartosc_Talii( ) );
                            konto = konto - zaklad;
                            Runda = true;
                            break;
                        }
                    }
                    if ( a == 2 ) {
                        break;
                    }
                }
                System.out.println( "Reka krupiera:" + Krupier.toString( ) );
                if ( ( Krupier.Wartosc_Talii( ) > Reka.Wartosc_Talii( ) ) && Runda == false ) {
                    System.out.println( "Przegrales. Wynik: " + Krupier.Wartosc_Talii( ) + " do: " + Reka.Wartosc_Talii( ) );
                    konto = konto - zaklad;
                    Runda = true;
                }
                while ( ( Krupier.Wartosc_Talii( ) < 17 ) && Runda == false ) {
                    Krupier.Dobierz( bierzaca_talia );
                    System.out.println( "Krupier dobiera: " + Krupier.zwroc_karte( Krupier.rozmiar_talii( ) - 1 ).toString( ) );
                }
                System.out.println( "Wartosc reki krupiera: " + Krupier.Wartosc_Talii( ) );
                if ( ( Krupier.Wartosc_Talii( ) > 21 ) && Runda == false ) {
                    System.out.println( "Wygrales. " );
                    Runda = true;
                    konto = konto + zaklad;
                }
                if ( ( Krupier.Wartosc_Talii( ) == Reka.Wartosc_Talii( ) ) && Runda == false ) {
                    System.out.println( "Remis ! Wygrywa krupier." );
                    konto=konto-zaklad;
                    Runda = true;
                }
                if ( ( Reka.Wartosc_Talii( ) > Krupier.Wartosc_Talii( ) ) && Runda == false ) {
                    System.out.println( "Wygrywasz rozdanie." );
                    konto = konto + zaklad;
                    Runda = true;

                } else if ( Runda == false ) {
                    System.out.println( "Krupier wygrywa. " );
                    konto = konto - zaklad;
                }
                Reka.pobierz_karte ( bierzaca_talia );
                Krupier.pobierz_karte ( bierzaca_talia );
                czy2 = false;
                System.out.println( "Koniec rozdania. Stan Twojego konta: " + konto );
                if ( konto > 0 ) {
                    System.out.println( " Chcesz grac dalej ?  (Wpisz: false - nie ; true - tak)" );
                    gramy = cin.nextBoolean( );
                } else {
                    System.out.println( "Niestety wszystko przegrales" );
                }
            }
            System.out.println ("Twoja wygrana to "+konto);
        }
        System.out.println ("Zeby grac musisz miec $$ !");
        System.out.println( "Koniec gry." );
        cin.close( );
    }
}