/**
 * Created by Krzysztof on 2017-03-14.
 */


import java.util.ArrayList;
import java.util.Random;
public class Talia /* extends Karta*/ {

    ArrayList <Karta> karty;

    public Talia () {
        this.karty = new ArrayList <Karta> ( );
    }

    public void Stworz_Talie () {
        for ( Kolor kolor : Kolor.values() ) {
            for ( Ranking ranking : Ranking.values ( ) ) {
                this.karty.add ( new Karta ( kolor, ranking ) );
            }
        }
    }

    public void tasuj () {
        ArrayList <Karta> k = new ArrayList <Karta> ( );
        Random generator = new Random ( );
        int j = 0;
        int rozmiar = this.karty.size ( );
        for ( int i = 0 ; i < rozmiar ; i++ ) {
            j = generator.nextInt ( ( this.karty.size ( ) - 1 - 0 ) + 1 ) + 0;
            k.add ( this.karty.get ( j ) );
            this.karty.remove ( j );
        }
        this.karty = k;
    }

    public void usun_karte ( int i ) {
        this.karty.remove ( i );
    }

    public Karta zwroc_karte ( int i ) {
        return this.karty.get ( i );
    }

    public void dodaj_karte ( Karta dodaj ) {
        this.karty.add ( dodaj );
    }

    public void Dobierz ( Talia dob ) {
        this.karty.add ( dob.zwroc_karte ( 0 ) );
        dob.usun_karte ( 0 );
    }

    public String toString () {
        String Lista_Kart = "";
        int i = 0;
        for ( Karta karta : this.karty ) {
            Lista_Kart += "\n" + karta.toString ( );
            i++;
        }
        return Lista_Kart;
    }

    public void pobierz_karte ( Talia zwr ) {
        int rozmiar = this.karty.size ( );
        for ( int i = 0 ; i < rozmiar ; i++ ) {
            zwr.dodaj_karte ( this.zwroc_karte ( i ) );
        }
        for ( int i = 0 ; i < rozmiar ; i++ ) {
            this.usun_karte ( 0 );
        }
    }

    public int rozmiar_talii () {
        return this.karty.size ( );
    }

    public int Wartosc_Talii () {
        int wartosc = 0;
        int as = 0;
        for ( Karta karta : this.karty ) {
            switch ( karta.wartosc ( ) ) {
                case TWO:
                    wartosc += 2;
                    break;
                case THREE:
                    wartosc += 3;
                    break;
                case FOUR:
                    wartosc += 4;
                    break;
                case FIVE:
                    wartosc += 5;
                    break;
                case SIX:
                    wartosc += 6;
                    break;
                case SEVEN:
                    wartosc += 7;
                    break;
                case EIGHT:
                    wartosc += 8;
                    break;
                case NINE:
                    wartosc += 9;
                    break;
                case TEN:
                    wartosc += 10;
                    break;
                case J:
                    wartosc += 10;
                    break;
                case Q:
                    wartosc += 10;
                    break;
                case K:
                    wartosc += 10;
                    break;
                case A:
                    as += 1;
                    break;
            }
        }
        for ( int i = 0 ; i < as ; i++ ) {
            if ( wartosc > 10 ) {
                wartosc += 1;
            } else {
                wartosc += 11;
            }
        }
        return wartosc;

    }
}
