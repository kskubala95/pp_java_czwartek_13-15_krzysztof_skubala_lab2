/**
 * Created by Krzysztof on 2017-03-15.
 */

public enum Ranking {
    A,
    K,
    Q,
    J,
    TEN,
    NINE,
    EIGHT,
    SEVEN,
    SIX,
    FIVE,
    FOUR,
    THREE,
    TWO
}
